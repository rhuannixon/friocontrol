const Employee = require("../models/Employee");
module.exports = {
  async index(req, res) {
    Employee.findAll({
      include: ["Occupation", "User"]
    })
      .then(employee => res.send(employee))
      .catch(err => res.send(err));
  },

  async insert(req, res) {
    let { occupationId, name } = req.body;

    if (!occupationId) {
      return res.send({ error: "Campo ocupação está inválido" });
    }

    if (!name) {
      return res.send({ error: "Campo Nome está inválido" });
    }

    Employee.create(req.body)
      .then(employee => res.send(employee))
      .catch(err => res.status(500).send(err.message));
  },

  async edit(req, res) {
    try {
      let { id, name } = req.body;
      let employee = await Employee.findByPk(id);

      if (!employee) {
        res.send({ error: "Funcionário não encontrado" });
      }

      employee.name = name;
      employee.save();

      res.send({ message: employee });
    } catch (e) {
      res.send({ error: employee });
    }
  }
};
