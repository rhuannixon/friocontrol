var Measure = require('../models/Measure');

exports.getAll = function(req, res) {
    Measure.findAll()
        .then(measure => res.send(measure))
        .catch(err => res.send(err));
}

exports.insert = function(req, res) {
    Measure.create(req.body)
        .then(measure => res.send(measure))
        .catch(err => res.status(201).send({ error: err }));
};