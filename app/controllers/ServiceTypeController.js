const Service = require('../models/Service');
const ServiceType = require('../models/ServiceType');

module.exports = {
  async index(req, res) {
    const { serviceId } = req.params;

    const service = await ServiceType.findByPk(serviceId, {
      include: { 
        association: 'services', 
        through: { 
          attributes: []
        } 
      }
    })

    return res.json(service.serviceType);
  },

  async store(req, res) {
    try{
      
      const { serviceId } = req.params;
      const { description, price } = req.body;
      
      const service = await Service.findByPk(serviceId);
      
      if (!service) {
        return res.status(400).json({ error: 'Service not found' });
      }
      
      const [ servicetype ] = await ServiceType.findOrCreate({
        where: { description ,price}
      });
      await service.addTypes(servicetype);
      
      return res.json(servicetype);
    }catch(err){
      console.log(err)
      res.json(err)
    }
  }
};