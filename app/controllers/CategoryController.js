const Category = require("../models/Category");

module.exports = {
  async getAll(req, res) {
    try {
      var category = await Category.findAll();
      res.send(category);
    } catch (err) {
      console.log(err);
      res.send(err);
    }
  },

  async insert(req, res) {
    try {
      var category = await Category.create(req.body);
      res.send(category);
    } catch (err) {
      res.status(201).send({ error: err });
    }
  },

  async delete(req, res) {
    try {
      let result = await Category.destroy(req.params.id);
      res.send({ message: "Excluído com sucesso" });
    } catch (e) {
      res.send({ error: e.message });
    }
  }
};
