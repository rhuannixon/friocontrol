const Consumer = require('../models/Consumer');

module.exports = {
    async getAll(req, res) {
        try{
            var consumer = await Consumer.findAll();
                res.send({ consumer });
            }
        catch(err){
            console.log(err)
            res.status(500).send({ error: err });
        } 
    },

    async insert(req, res) {
        try{
            var consumer = await Consumer.create(req.body);
                res.send(consumer);
                
            }catch(err){
                res.status(201).send({ error: err });

        }
    }
}

