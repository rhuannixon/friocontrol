const Tool = require("../models/Tool");
const LoanTool = require("../models/LoanTool");
const Employee = require("../models/Employee");
var Sequelize = require("sequelize");
const Op = Sequelize.Op;

module.exports = {
  async index(req, res) {
    try {
      const tools = await Tool.findAll({});

      return res.json(tools);
    } catch (err) {
      console.log(err);
      return res.status(500).send(err);
    }
  },

  async toolsAvailable(req, res) {
    try {
      const ids = [];
      const loansId = await LoanTool.findAll({
        where: {
          devolution: false
        },
        attributes: ["toolId"]
      });

      loansId.map(id => {
        ids.push(id.toolId);
      });

      if (loansId.length === 0) {
        loansId.push(0);
      }

      const tools = await Tool.findAll({
        include: [
          {
            association: "loans",

            where: {
              devolution: true,
              toolId: {
                [Op.notIn]: ids
              }
            },
            attributes: []
          }
        ]
      });

      return res.json(tools);
    } catch (err) {
      console.log(err);
      return res.status(500).send(err);
    }
  },

  async store(req, res) {
    try {
      const tool = await Tool.create(req.body);
      const fakeLoan = await LoanTool.create({
        employeeId: 1,
        devolution: true,
        toolId: tool.id
      });
      res.json(tool);
    } catch (err) {
      console.log(err);
      res.status(500).send(err);
    }
  },

  async loan(req, res) {
    try {
      const { employeeId, toolId } = req.body;
      console.log(`Verificando se ferramenta(id:${toolId}) existe.`);
      var tool = await Tool.findByPk(toolId);

      if (!tool) {
        console.log(`Ferramenta (id:${toolId}) não existe.`);
        res.status(400).send({ msg: `Ferramenta (id:${toolId}) não existe.` });
      }

      console.log(`Verificando se a ferramenta já está emprestada.`);
      var loan = await LoanTool.findOne({
        where: {
          toolId: toolId,
          devolution: false
        }
      });

      if (loan) {
        console.log(`Esta ferramenta já foi emprestada.`);
        return res
          .status(400)
          .send({ msg: `Esta ferramenta já foi emprestada.` });
      }
      console.log("Verificando se o funcionário existe.");
      var employee = await Employee.findByPk(employeeId);

      await LoanTool.create({
        employeeId: employee.id,
        toolId: tool.id
      });

      return res.status(200).send("Empréstimo cadastrado com sucesso!");
    } catch (err) {
      return res.status(500).send(err);
    }
  },

  async listLoan(req, res) {
    try {
      var tools = await LoanTool.findAll({
        include: [
          {
            model: Employee
          },
          {
            model: Tool
          }
        ],
        order: [["updatedAt", "DESC"]]
      });
      res.status(200).send(tools);
    } catch (err) {
      console.log(err);
      res.status(500).send(err);
    }
  },

  async returnTool(req, res) {
    try {
      var loan = await LoanTool.findByPk(req.params.loanId);

      if (!loan) {
        console.log("Esta ferramenta já foi devolvida.");
        res.status(500).send("Esta ferramenta já foi devolvida.");
      }

      loan.devolution = true;
      loan.save();

      res.status(200).send({ msg: `Ferramenta devolvida com sucesso.` });
    } catch (err) {
      console.log(err);
      res.status(500).send(err);
    }
  },

  async indexLoans(req, res) {
    try {
      const loans = await LoanTool.findAll({
        where: {
          devolution: false
        },
        include: [
          {
            model: Tool
          },
          {
            model: Employee
          }
        ]
      });
      return res.status(200).send(loans);
    } catch (err) {
      console.log(err);
    }
  },

  async delete(req, res) {
    try {
      const tool = await Tool.findByPk(req.params.id);

      if (!tool) {
        res
          .status(400)
          .send(`Ferramenta id:${req.params.toolId} não encontrada.`);
      }

      const result = await tool.destroy();

      res.status(200).send(result);
    } catch (err) {
      res.status(500).send(err);
    }
  },

  async edit(req, res) {
    try {
      let { id, description } = req.body;
      let tool = await Tool.findByPk(id);

      if (!tool) {
        res.send({ error: "Usuário não encontrado." });
      }
      tool.description = description;
      tool.save();
      res.send({ message: tool });
    } catch (e) {
      res.send({ error: e.message });
    }
  }
};
