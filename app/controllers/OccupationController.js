const Occupation = require('../models/Occupation');

module.exports = {

    async getAll(req,res) {
        try{
            var occupation = await Occupation.findAll();
            return res.send({occupation});
        }
        catch(err){
            console.log(err);
            return res.send({error:err});
        } 
    },

    async insert(req,res) {
        try {
            var occupation = await Occupation.create(req.body);
            return res.send(occupation);
        }
        catch(err){
            return res.status(201).send({error:err});
        } 
    }
}