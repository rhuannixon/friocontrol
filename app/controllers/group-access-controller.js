var {GroupAccess,UsersGroupAccesses} = require('../models/index');

exports.getAll = function(req,res){
    GroupAccess.findAll()
    .then(groupAccess => res.send({groupAccess}))
    .catch(err => res.send({error:err}));
}

exports.insert = function(req,res) {
    GroupAccess.create(req.body)
    .then(groupAccess => res.send({groupAccess}))
    .catch(err => res.status(201).send({error:err}));
};