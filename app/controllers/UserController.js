const User = require("../models/User");
const auth = require("../util/auth");
const encrypt = require("../util/encrypt");

module.exports = {
  async getAll(req, res) {
    console.log("searching for users...");
    try {
      var user = await User.findAll({
        include: ["Employee", "GroupAccess"]
      });
      res.send({ user });
    } catch (err) {
      console.log(err);
      res.send({ error: err });
    }
  },

  async insert(req, res) {
    try {
      const { groupAccesses, ...data } = req.body;

      var user = await User.create(data);
      token = await auth.generateAuthToken(user.id);
      user.token = token;
      user.save();
      res.setHeader("Bearer", token);

      if (groupAccesses && groupAccesses.length > 0) {
        user.setGroupAccesses(groupAccesses);
      }

      return res.status(201).json(user);
    } catch (err) {
      console.log(err);
      return res.status(500).json({ err });
    }
  },

  async signup(req, res) {
    try {
      const user = await User.create(req.body);
      token = await auth.generateAuthToken(user.id);
      user.token = token;
      user.save();
      res.setHeader("Bearer", token);
      res.status(201).json(user);
    } catch (err) {
      console.log(err);
      res.status(500).send(err);
    }
  },

  async signin(req, res) {
    try {
      const user = await User.findOne({ login: req.body.login });
      if (user) {
        isValid = encrypt.validPassword(req.body.password, user.password);
        if (isValid) {
          token = await auth.generateAuthToken(user.id);
          user.token = token;
          user.save();
          res.setHeader("Bearer", token);
          return res.status(200).json(user);
        }
      }
      return res.send({
        message: null,
        error: "Usuário e/ou senha inválidos",
        statusCode: 401
      });
    } catch (err) {
      res.status(500).send(err);
    }
  },

  async search(req, res) {
    try {
      var token = req.headers.bearer;
      if (!token) return res.status(401).send("Não autorizado");

      auth.verify(req, res, token);

      user = await User.findOne({
        where: {
          id: req.params.id
        },
        include: ["Telefones"]
      });

      if (!user) {
        res.status(404).send("Usuário não encontrado.");
      }

      res.status(200).send(user);
    } catch (err) {
      console.log(err);
      res.status(500).send(err);
    }
  }
};
