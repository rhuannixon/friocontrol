const Product = require("../models/Product");
const SaleProduct = require("../models/SaleProduct");
const PurchaseProduct = require("../models/PurchaseProduct");
const Stock = require("../models/Stock");
const Category = require("../models/Category");
const Measure = require("../models/Measure");

module.exports = {
  async getAll(req, res) {
    try {
      var product = await Product.findAll({
        include: ["Measure", "Category"]
      });
      res.send(product);
    } catch (err) {
      console.log(err);
      res.status(500).send({ error: err });
    }
  },

  async getById(req, res) {
    try {
      let product = await Product.findByPk(req.params.id);

      if (!product) return res.send({ error: "Produto não encontrado" });
      res.send(product);
    } catch (err) {
      console.log(err);
      res.status(500).send({ error: err });
    }
  },

  async insert(req, res) {
    try {
      let cat = await Category.findByPk(req.body.categoryId);
      if (!cat) res.send({ error: "Categoria inválida." });
      let measure = await Category.findByPk(req.body.measureId);
      if (!measure) res.send({ error: "Medida inválida." });
      var product = await Product.create(req.body);
      await Stock.create({
        productId: product.id,
        qtd: 0
      });
      return res.send(product);
    } catch (err) {
      console.log(err);
      res.status(500).send({ error: err });
    }
  },

  async sales(req, res) {
    try {
      var sales = await SaleProduct.findAll({
        include: ["product"],
        order: [["createdAt", "DESC"]]
      });
      res.status(200).send(sales);
    } catch (err) {
      console.log(err);
      res.status(500).send(err);
    }
  },

  async insertSale(req, res) {
    try {
      console.log("Verificando se o produto existe.");
      var product = await Product.findByPk(req.body.productId);

      if (!product) {
        console.log("Produto não existe.");
        return res.send({ error: "Produto não existe." });
      }

      console.log(`Verificando se o produto existe no estoque: ${stock}`);
      var stock = await Stock.findOne({
        where: {
          productId: req.body.productId
        }
      });

      if (stock) {
        if (stock.qtd - req.body.qtd < 0) {
          return res.status(400).send({
            msg: "Quantidade solicitada é maior que a existente no estoque."
          });
        }
      } else {
        return res.status(400).send({ msg: "Produto não existe no estoque." });
      }
      var sale = await SaleProduct.create(req.body);
      console.log(`Compra registrada(${sale})`);

      var stockAmount = stock.qtd - sale.qtd;
      stock.qtd = stockAmount;
      await stock.save();

      res.status(200).send(sale);
    } catch (err) {
      console.log(err);
      res.status(500).err(err.message);
    }
  },

  async purchases(req, res) {
    try {
      var purchase = await PurchaseProduct.findAll({
        include: [
          {
            model: Product
          }
        ],
        order: [["createdAt", "DESC"]]
      });
      res.status(200).send(purchase);
    } catch (err) {
      console.log(err);
      res.status(500).err(err.message);
    }
  },

  async insertPurchase(req, res) {
    try {
      console.log("Verificando se o produto existe.");
      var product = await Product.findByPk(req.body.productId);

      if (!product) {
        console.log("Produto não existe.");
        res.send({ error: "Produto não existe." });
      }

      var purchase = await PurchaseProduct.create(req.body);
      console.log(`Compra registrada(${purchase})`);

      var stock = await Stock.findOne({
        where: {
          productId: purchase.productId
        }
      });
      console.log(`Returning products in stock: ${stock}`);

      if (!stock) {
        console.log(`Creating products in stock`);
        stock = await Stock.create({
          productId: purchase.productId,
          qtd: 0
        });
      }

      var stockAmount = parseInt(stock.qtd) + parseInt(purchase.qtd);
      stock.qtd = stockAmount;
      await stock.save();

      res.send({ message: purchase });
    } catch (err) {
      console.log(err.message);
      res.send({ error: err.message });
    }
  },

  async stock(req, res) {
    try {
      var stock = await Stock.findAll({
        include: [
          {
            model: Product
          }
        ]
      });
      res.status(200).send(stock);
    } catch (err) {
      console.log(err);
      res.status(500).err(err);
    }
  },

  async deleteProduct(req, res) {
    try {
      let prod = await Product.findByPk(req.params.productId);

      if (!prod) {
        res
          .status(400)
          .send(`Produto id:${req.params.productId} não encontrado.`);
      }

      let result = await prod.destroy();
      res.status(200).send(result);
    } catch (e) {
      console.log(e);
      res.status(500).send(e);
    }
  },

  async edit(req, res) {
    try {
      let id = req.params.id;
      let { measureId, categoryId, description, price } = req.body;
      let product = await Product.findByPk(id);
      if (!product) {
        return res.send({ error: "Usuário não existe." });
      }

      if (measureId) {
        product.measureId = measureId;
      }
      if (categoryId) {
        product.categoryId = categoryId;
      }

      if (description) {
        product.description = description;
      }

      if (price) {
        product.price = price;
      }

      var result = await product.save();

      return res.send({ message: result });
    } catch (error) {
      return res.send({ error: error.message });
    }
  }
};
