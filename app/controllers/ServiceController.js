const Service = require("../models/Service");
const Product = require("../models/Product");
const Consumer = require("../models/Consumer");
const SaleProduct = require("../models/SaleProduct");
const ServiceSaleProduct = require("../models/ServiceSaleProduct");
const Stock = require("../models/Stock");

module.exports = {
  async index(req, res) {
    try {
      const services = await Service.findAll({
        include: [
          {
            model: Consumer,
            as: "consumer"
          },
          {
            model: SaleProduct,
            as: "products",
            include: ["product"]
          }
        ]
      });

      return res.json(services);
    } catch (err) {
      console.log(err);
      return res.status(500).send(err);
    }
  },

  async indexProducts(req, res) {
    try {
      const services = await ServiceSaleProduct.findAll({
        include: ["Service", "SaleProduct"]
      });

      return res.json(services);
    } catch (err) {
      console.log(err);
      return res.status(500).send(err);
    }
  },

  async store(req, res) {
    try {
      var { products, ...data } = req.body;
      console.log("Checking if product exist.");
      for (var i = 0, len = products.length; i < len; ++i) {
        var prod = products[i];
        var product = await Product.findByPk(prod.productId);
        if (!product) {
          res.send({
            error: `O produto com id : ${prod.productId} não existe.`
          });
        }
      }
      const service = await Service.create(data);
      console.log(`Serviço criado: id:${service.id}`);

      for (var i = 0, len = products.length; i < len; ++i) {
        var prod = products[i];
        var stock = await Stock.findOne({
          where: {
            productId: prod.productId
          }
        });

        if (stock) {
          console.log(`Verificando se o produto existe no estoque: ${stock}`);
          if (stock.qtd - prod.qtd < 0) {
            return res.send({
              error: "Quantidade solicitada é maior que a existente no estoque."
            });
          }
        } else {
          return res.send({ error: "Produto não existe no estoque." });
        }

        var sale = await SaleProduct.create({
          productId: prod.productId,
          qtd: prod.qtd,
          salePrice: prod.salePrice,
          serviceId: service.id
        });

        var stockAmount = parseInt(stock.qtd) - parseInt(sale.qtd);
        stock.qtd = stockAmount;
        await stock.save();
      }
      console.log(`Produtos inseridos!`);

      return res.json(service);
    } catch (err) {
      console.log(err);
      return res.status(500).send(err.message);
    }
  }
};
