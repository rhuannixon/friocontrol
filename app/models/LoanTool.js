const { Model, DataTypes } = require('sequelize');

class LoanTool extends Model {
    static init(sequelize) {
        super.init({
            toolId: DataTypes.INTEGER,
            employeeId: DataTypes.INTEGER,
            devolution:DataTypes.BOOLEAN
        }, {
            sequelize
        })
    }

    static associate(models) {
        this.belongsTo(models.Tool);
        this.belongsTo(models.Employee);
    }

}

module.exports = LoanTool;