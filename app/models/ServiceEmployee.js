const { Model, DataTypes } = require('sequelize');

class ServiceEmployee extends Model {
    static init(sequelize) {
        super.init({
            serviceId:DataTypes.INTEGER,
            employeeId:DataTypes.INTEGER,
        }, {
            sequelize
        })
    }

    static associate(models) {
        this.belongsTo(models.Service);
        this.belongsTo(models.Employee);
    }
}

module.exports = ServiceEmployee;