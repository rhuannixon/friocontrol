const { Model, DataTypes } = require('sequelize');

class Measure extends Model {
    static init(sequelize) {
        super.init({
            description: DataTypes.STRING
        }, {
            sequelize
        })
    }

    static associate(models) {
        this.hasOne(models.Product);
    }
}

module.exports = Measure;