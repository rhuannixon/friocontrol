const { Model, DataTypes } = require("sequelize");

class Product extends Model {
  static init(sequelize) {
    super.init(
      {
        code: DataTypes.STRING,
        //active:DataTypes.BOOLEAN
        description: DataTypes.STRING,
        price: DataTypes.DECIMAL(10, 2),
        categoryId: DataTypes.INTEGER,
        measureId: DataTypes.INTEGER
      },
      {
        sequelize
      }
    );
  }

  static associate(models) {
    this.belongsTo(models.Category);
    this.belongsTo(models.Measure);
    this.belongsToMany(models.Service, {
      foreignKey: "productId",
      through: "serviceproduct",
      as: "service"
    });
  }
}

module.exports = Product;
