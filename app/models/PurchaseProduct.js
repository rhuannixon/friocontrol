const { Model, DataTypes } = require('sequelize');

class PurchaseProduct extends Model {
    static init(sequelize) {
        super.init({
            productId:DataTypes.INTEGER,
            qtd: DataTypes.INTEGER,
            purchasePrice: DataTypes.DECIMAL(9,2)
        }, {
            sequelize
        })
    }

    static associate(models) {
        this.belongsTo(models.Product);
    }
}

module.exports = PurchaseProduct;