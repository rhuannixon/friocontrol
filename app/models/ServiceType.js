const { Model, DataTypes } = require('sequelize');

class ServiceType extends Model {
  static init(sequelize) {
    super.init({
      description: DataTypes.STRING,
      price: DataTypes.DECIMAL(9, 2),
    }, {
      sequelize
    })
  }

  static associate(models) {
    this.belongsToMany(models.Service, { foreignKey: 'serviceTypeId', through: 'typeservice', as: 'services' });
  }
}

module.exports = ServiceType;