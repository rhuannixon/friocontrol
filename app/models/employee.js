const { Model, DataTypes } = require('sequelize');

class Employee extends Model {
    static init(sequelize) {
        super.init({
            name: DataTypes.STRING,
            bDate: DataTypes.DATE,
            registration: DataTypes.STRING,
            phone: DataTypes.STRING,
            occupationId:DataTypes.INTEGER
        }, {
            sequelize
        })
    }

    static associate(models) {
        this.belongsTo(models.Occupation);
        this.hasOne(models.User);
        this.belongsToMany(models.Tool, { foreignKey: 'employeeId', through: 'loantools', as: 'tools' });
    }
}

module.exports = Employee;