const { Model, DataTypes } = require('sequelize');

class Tool extends Model {
    static init(sequelize) {
        super.init({
            description: DataTypes.INTEGER
        }, {
            sequelize
        })
    }

    static associate(models) {
        this.belongsToMany(models.Employee, { foreignKey: 'toolId', through: 'loantools', as: 'employees' });
        this.hasMany(models.LoanTool,{as:'loans'});
    }
}

module.exports = Tool;