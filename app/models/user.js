const bcrypt = require("bcrypt");
const { Model, DataTypes } = require('sequelize');

class User extends Model {
    static init(sequelize) {
        super.init({
            name: DataTypes.STRING,
            login: DataTypes.STRING,
            password: DataTypes.STRING,
            token: DataTypes.STRING,
            employeeId: DataTypes.INTEGER,
            groupAccessId: DataTypes.INTEGER
        }, {
            hooks: {
                beforeCreate: (user) => {
                    const salt = bcrypt.genSaltSync();
                    user.password = bcrypt.hashSync(user.password, salt);
                }
            },
            sequelize
        })
    }

    static associate(models) {
        this.belongsTo(models.Employee)
        this.belongsTo(models.GroupAccess)
    }
}

module.exports = User;