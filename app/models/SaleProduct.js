const { Model, DataTypes } = require('sequelize');

class SaleProduct extends Model {
    static init(sequelize) {
        super.init({
            productId:DataTypes.INTEGER,
            serviceId:DataTypes.INTEGER,
            qtd: DataTypes.INTEGER,
            salePrice: DataTypes.DECIMAL(9,2)
        }, {
            sequelize
        })
    }

    static associate(models) {
        this.belongsTo(models.Product,{foreignKey:'productId',as:'product'});
        this.belongsTo(models.Service);
    }
}

module.exports = SaleProduct;