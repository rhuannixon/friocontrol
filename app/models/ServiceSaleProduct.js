const { Model, DataTypes } = require('sequelize');

class ServiceSaleProduct extends Model {
    static init(sequelize) {
        super.init({
            serviceId:DataTypes.INTEGER,
            saleProductId:DataTypes.INTEGER,
        }, {
            sequelize
        })
    }

    static associate(models) {
        this.belongsTo(models.Service);
        this.belongsTo(models.SaleProduct);
    }
}

module.exports = ServiceSaleProduct;