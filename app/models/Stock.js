const { Model, DataTypes } = require('sequelize');

class Stock extends Model {
    static init(sequelize) {
        super.init({
            productId:DataTypes.INTEGER,
            qtd: DataTypes.INTEGER,
            //status,
            //observacao
        }, {
            sequelize
        })
    }

    static associate(models) {
        this.belongsTo(models.Product);
    }
}

module.exports = Stock;