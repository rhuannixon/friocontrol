const { Model, DataTypes } = require('sequelize');

class Occupation extends Model {
    static init(sequelize) {
        super.init({
            description: DataTypes.STRING
        }, {
            sequelize
        })
    }

    static associate(models) {
        this.hasOne(models.Employee);
    }
}

module.exports = Occupation;