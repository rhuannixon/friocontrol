const { Model, DataTypes } = require('sequelize');

class Service extends Model {
    static init(sequelize) {
        super.init({}, {
            sequelize
        })
    }

    static associate(models) {
        this.hasMany(models.ServiceEmployee);
        this.belongsTo(models.Consumer, { as: 'consumer' });
        this.belongsToMany(models.ServiceType, { foreignKey: 'serviceId', through: 'typeservice', as: 'types' });
        this.hasMany(models.SaleProduct, {foreignKey:'serviceId', as:'products'});
    }
}

module.exports = Service;