const { Model, DataTypes } = require('sequelize');

class GroupAccess extends Model {
    static init(sequelize) {
        super.init({
            description: DataTypes.STRING
        }, {
            sequelize
        })
    }

    static associate(models) {
        this.hasOne(models.User);
    }
}

module.exports = GroupAccess;