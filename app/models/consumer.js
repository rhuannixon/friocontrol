const { Model, DataTypes } = require('sequelize');

class Consumer extends Model {
    static init(sequelize) {
        super.init({
            name: DataTypes.STRING,
            phone: DataTypes.STRING
        }, {
            sequelize
        })
    }

    static associate(models) {
        this.hasMany(models.Service, { foreignKey: 'consumerId', as: 'service' });
    }
}

module.exports = Consumer;