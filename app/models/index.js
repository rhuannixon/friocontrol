const Sequelize = require('sequelize');
const dbConfig = require('../../config/database');

const Service = require('./Service');
const ServiceType = require('./ServiceType');
const Consumer = require('./Consumer');
const Tool = require('./Tool');
const Employee = require('./Employee');
const Occupation = require('./Occupation');
const User = require('./User');
const GroupAccess = require('./GroupAccess');
const Category = require('./Category');
const Product = require('./Product');
const Measure = require('./Measure');
const SaleProduct = require('./SaleProduct');
const Stock = require('./Stock');
const PurchaseProduct = require('./PurchaseProduct');
const LoanTool = require('./LoanTool');
const ServiceSaleProduct = require('./ServiceSaleProduct');
const ServiceEmployee = require('./ServiceEmployee');

const connection = new Sequelize(dbConfig);

//-------init--------\\
Service.init(connection);
ServiceType.init(connection);
Consumer.init(connection);
Tool.init(connection);
Employee.init(connection);
Occupation.init(connection);
User.init(connection);
GroupAccess.init(connection);
Category.init(connection);
Product.init(connection);
LoanTool.init(connection);
Measure.init(connection);
SaleProduct.init(connection);
PurchaseProduct.init(connection);
Stock.init(connection);
ServiceSaleProduct.init(connection);
ServiceEmployee.init(connection);

//-------associate--------\\
Service.associate(connection.models);
ServiceType.associate(connection.models);
Consumer.associate(connection.models);
Tool.associate(connection.models);
Employee.associate(connection.models);
Occupation.associate(connection.models);
User.associate(connection.models);
GroupAccess.associate(connection.models);
Category.associate(connection.models);
Product.associate(connection.models);
Measure.associate(connection.models);
SaleProduct.associate(connection.models);
Stock.associate(connection.models);
PurchaseProduct.associate(connection.models);
LoanTool.associate(connection.models);
ServiceSaleProduct.associate(connection.models);
ServiceEmployee.associate(connection.models);

module.exports = connection;