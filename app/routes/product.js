var express = require("express");
var router = express.Router();

var controller = require("../controllers/ProductController");

router.get("/", controller.getAll);
router.post("/", controller.insert);
router.delete("/:productId", controller.deleteProduct);
router.get("/stock", controller.stock);
router.get("/sale", controller.sales);
router.post("/sale", controller.insertSale);
router.get("/purchase", controller.purchases);
router.post("/purchase", controller.insertPurchase);
router.get("/:id", controller.getById);
router.put("/:id", controller.edit);

module.exports = router;
