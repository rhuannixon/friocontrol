var express = require("express");
var router = express.Router();

var controller = require("../controllers/CategoryController");

router.get("/", controller.getAll);
router.post("/", controller.insert);
router.delete("/:id", controller.delete);

module.exports = router;
