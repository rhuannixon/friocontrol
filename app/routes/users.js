var express = require('express');
var router = express.Router();

var controller = require('../controllers/UserController');

router.get('/list', controller.getAll);

router.post('/insert', controller.insert);

router.post('/signup', controller.signup);

router.post('/signin', controller.signin);

router.get('/search/:id', controller.search);

module.exports = router;