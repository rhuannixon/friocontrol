var express = require('express');
var router = express.Router();

var controller = require('../controllers/group-access-controller');

router.get('/list',controller.getAll);
router.post('/insert',controller.insert);


module.exports=router;