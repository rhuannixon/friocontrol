var express = require("express");
var router = express.Router();

var controller = require("../controllers/ToolController");

router.get("/", controller.index);
router.post("/", controller.store);
router.get("/available", controller.toolsAvailable);
router.get("/loan", controller.listLoan);
router.post("/loan", controller.loan);
router.post("/loan/devolution/:loanId", controller.returnTool);
router.get("/loan/list", controller.indexLoans);
router.delete("/:id", controller.delete);
router.put("/", controller.edit);

module.exports = router;
