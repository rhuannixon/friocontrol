var express = require("express");
var router = express.Router();

var controller = require("../controllers/EmployeeController");

router.get("/", controller.index);
router.post("/", controller.insert);
router.put("/", controller.edit);

module.exports = router;
