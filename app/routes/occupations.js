var express = require('express');
var router = express.Router();

var controller = require('../controllers/OccupationController');

router.get('/list',controller.getAll);
router.post('/insert',controller.insert);


module.exports=router;