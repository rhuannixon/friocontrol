var express = require('express');
var router = express.Router();

var controller = require('../controllers/measure-controller');

router.get('/', controller.getAll);
router.post('/', controller.insert);


module.exports = router;