var express = require('express');
var router = express.Router();

var controller = require('../controllers/ServiceController');

router.get('/', controller.index);
router.get('/products', controller.indexProducts);
router.post('/', controller.store);


module.exports = router;