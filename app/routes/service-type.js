var express = require('express');
var router = express.Router();

var controller = require('../controllers/ServiceTypeController');

router.get('/', controller.index);
router.post('/:serviceId', controller.store);


module.exports = router;