const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const helmet = require("helmet");
const path = require("path");
const index = require("./app/routes/index");
const occupation = require("./app/routes/occupations");
const employee = require("./app/routes/employees");
const user = require("./app/routes/users");
const groupAccess = require("./app/routes/group-access");
const category = require("./app/routes/category");
const measure = require("./app/routes/measure");
const product = require("./app/routes/product");
const serviceType = require("./app/routes/service-type");
const service = require("./app/routes/service");
const tool = require("./app/routes/tool");
const consumer = require("./app/routes/consumer");
const app = express();
const { autheticate } = require("./services/session");
autheticate.unless = require("express-unless");

port = process.env.PORT || 3002;

app.use(express.static(path.join(__dirname, "build")));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());
app.use(helmet());
app.use(
  autheticate.unless({
    path: [
      { url: "/", methods: ["GET"] },
      { url: "/user/signin", methods: ["POST"] }
    ]
  })
);

app.use("/", index);
app.use("/user", user);
app.use("/occupation", occupation);
app.use("/employee", employee);
app.use("/groupAccess", groupAccess);
app.use("/category", category);
app.use("/measure", measure);
app.use("/product", product);
app.use("/serviceType", serviceType);
app.use("/service", service);
app.use("/tool", tool);
app.use("/consumer", consumer);

app.listen(port);
console.log(`starting server on port: ${port}`);
