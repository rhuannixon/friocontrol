module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.bulkInsert('Occupations', [{
        description: 'admin',
        createdAt: new Date(),
        updatedAt: new Date()
    }], {}),

    down: (queryInterface) => queryInterface.bulkDelete('Occupations', null, {}),
};