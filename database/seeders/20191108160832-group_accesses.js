module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.bulkInsert('GroupAccesses', [{
        description: 'admin',
        createdAt: new Date(),
        updatedAt: new Date()
    }], {}),

    down: (queryInterface) => queryInterface.bulkDelete('GroupAccesses', null, {}),
};