module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.bulkInsert('Employees', [{
        name: 'Rhuan',
        bDate: '2019-01-29',
        registration: '123456',
        phone: '81999999999',
        occupationId: 1,
        createdAt: new Date(),
        updatedAt: new Date()
    }], {}),

    down: (queryInterface) => queryInterface.bulkDelete('Employees', null, {}),
};