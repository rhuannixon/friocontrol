module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.bulkInsert(
      "Categories",
      [
        {
          description: "Peça de estoque",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          description: "Material de insumo",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          description: "Material de limpeza",
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ],
      {}
    ),

  down: queryInterface => queryInterface.bulkDelete("Categories", null, {})
};
