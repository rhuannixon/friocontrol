module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('Products', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            code: {
                allowNull:false,
                type: Sequelize.STRING
            },
            active: {
                type: Sequelize.BOOLEAN,
                defaultValue: false
            },
            description: {
                allowNull: false,
                type: Sequelize.STRING(100),
                unique: true,
            },
            price: {
                allowNull: true,
                type: Sequelize.DECIMAL(10,2),
            },
            categoryId: {
                type: Sequelize.INTEGER,
                allowNull: false,
                references: {
                    model: 'Categories',
                    key: 'id'
                }
            },
            measureId: {
                type: Sequelize.INTEGER,
                allowNull: false,
                references: {
                    model: 'Measures',
                    key: 'id'
                }
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATEONLY,
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATEONLY,
            },
        });
    },

    down: (queryInterface) => {
        return queryInterface.dropTable('Products');
    }
};