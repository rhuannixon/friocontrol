'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('ServiceSaleProducts', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            serviceId: {
              type: Sequelize.INTEGER,
              allowNull: true,
              references: {
                  model: 'Services',
                  key: 'id'
              }
            },
            saleProductId: {
                type: Sequelize.INTEGER,
                allowNull: true,
                references: {
                    model: 'SaleProducts',
                    key: 'id'
                }
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
        });
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('ServiceSaleProducts');
    }
};