'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Employees', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      name: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      bDate: {
        allowNull: true,
        type: Sequelize.DATE,
      },
      registration: {
        allowNull: true,
        type: Sequelize.STRING,
        unique: true,
      },
      phone: {
        allowNull: true,
        type: Sequelize.STRING,
      },
      occupationId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Occupations',
          key: 'id'
        }
      },
      createdAt: {
			allowNull: false,
			type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Employees');  
  }
};
