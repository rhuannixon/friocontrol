'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('ServiceEmployees', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            employeeId: {
              type: Sequelize.INTEGER,
              allowNull: true,
              references: {
                  model: 'Employees',
                  key: 'id'
              }
            },
            servicetId: {
                type: Sequelize.INTEGER,
                allowNull: false,
                references: {
                    model: 'Services',
                    key: 'id'
                }
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
        });
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('ServiceEmployees');
    }
};