const jwt = require("jsonwebtoken");
require("dotenv/config");

exports.autheticate = (req, res, next) => {
  const token = req.headers.bearer;
  jwt.verify(token, process.env.API_KEY, function(err, decoded) {
    if (err)
      return res.send({
        message: "Não autorizado / Sessão expirada",
        error: "Unauthorized",
        statusCode: 401
      });
    next();
  });
};
